# Normal Stock Price Calculator

## Functional Programming B: Group 14

- Alsabila Shakina Prasetyo - 1606917613
- Ignatius Rahardi P. - 1606875951
- Justin - 1606878871
- Kevin Prakasa Anggono - 1606917696
- Swastinika Naima Moertadho - 1606821854

## Application Description

This application calculates the normal stock price for a certain company or asset. The normal stock price is based on the current stock price and certain data released in financial reports by said company.

## Methods of Valuating Stock

### Benjamin Graham Formula

This method was introduced by the father of value investing, Benjamin Graham, in his book The Intelligent Investor.

The original formula for Benjamin Graham method is this:

```
Value = (EPS x (8.5 + 2g) x 4.4) / Y

EPS: Earnings per share, gained by dividing net profit by amount of stock in the market.
g: Growth rate of company, capped at 15%.
Y: AAA corporate bond yield
```

Rivan Kurniawan (http://rivankurniawan.com/), an Indonesian value investor wrote about a version of Benjamin Graham formula, adjusted to the Indonesian market. The formula is as follows:

```
Value = (EPS x (7 + 1g) x 7.8) / Y

Y: AAA corporate bond yield for Indonesia is constant at 11.4%.
```

### PBV Analysis

- This method calculates from 5 years data of stock's PBV (Price Book Value). PBV (Price Book Value) is a ratio between stock's price and book value per share and Book value/share is the value of an asset which calculated from equity's value / total stock shares.

```
BVPS (Book Value per Share) = Equity's value / total shares (total lembar saham)
PBV = Stock's price / BVPS
```

PBV Category:

- PBV < 1 -> The stock is undervalued
- PBV = 1 -> The stock is on its normal value
- PBV > 1 -> The stock is overvalued

### PER Analysis

- This method is pretty similar with PBV Analysis, but in this case we use PER (Price Earning Ratio) which PER is a ratio between stock's price and EPS (Earnings Per Share) TTM

```
PER = Stock's price / EPS ttm
```

- We take all PER from five years back, and then take the mean from it. Use that mean to calculate the normal price of its stock.

### DCF Method

- This method explanations is pretty long, so it better you go to this reference directly [click me](https://www.tnp-capital.com/2015/02/cara-menghitung-harga-wajar-saham.html)
