import { createStore, applyMiddleware, compose } from "redux";
import { routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import createRootReducer from "./modules";
import { composeWithDevTools } from "redux-devtools-extension";

export const history = createHistory();

const initialState = {};
const enhancers : any = [];
const middleware = [thunk, routerMiddleware(history)];

const composedEnhancers = composeWithDevTools(
  applyMiddleware(...middleware),
  ...enhancers
);

const store = createStore(
  createRootReducer(history),
  initialState,
  composedEnhancers
);

export default store;
