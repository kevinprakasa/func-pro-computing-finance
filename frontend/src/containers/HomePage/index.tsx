import React, { Component } from 'react'
import { HomePageContainer } from './style'

class HomePage extends Component<any, any> {
  render() {
    return (
      <HomePageContainer>
        <h1>Hello World</h1>
      </HomePageContainer>
    )
  }
}

export default HomePage;