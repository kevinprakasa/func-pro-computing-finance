import styled from 'styled-components'

export const HomePageContainer = styled.div`
  h1 {
    font-size: 30px;
    color: ${props => props.theme.colors.main}
  }
`;