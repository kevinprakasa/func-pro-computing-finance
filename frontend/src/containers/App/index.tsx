import React, { Component } from 'react';
import { ThemeProvider } from "styled-components";
import { theme } from './theme';
import { routes } from "./routes";

import { Switch, Route, withRouter, Redirect } from "react-router-dom";

import { AppContainer } from "./style"

const App: React.FC = () => {
  const pages = routes.map((route) => {
    return (
      <Route
        component={route.component}
        exact={route.exact}
        path={route.path}
      />
    )
  })

  return (
    <ThemeProvider theme={theme}>
      <AppContainer>
        <Switch>{pages}</Switch>
      </AppContainer>
    </ThemeProvider>
  )
}

export default App;