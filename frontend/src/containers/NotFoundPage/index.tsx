import React from "react"

const NotFoundPage : React.FC = () => {
  return(
    <h1>404 page not found</h1>
  )
}

export default NotFoundPage;