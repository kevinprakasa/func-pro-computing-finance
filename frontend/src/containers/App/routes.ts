import HomePage from "containers/HomePage";
import NotFoundPage from "containers/NotFoundPage";

export const routes = [
  {
    component: HomePage,
    exact: true,
    path: "/"
  },
  {
    component: NotFoundPage
  }
]